Simple Python Program Manager package
=====================================

Installation
------------

### Distant

```bash
pip install git+https://gitlab.com/vepain/spyprogmanager@TAG
```

or (to be sure which python version you use)
```bash
python3.X -m pip install git+https://gitlab.com/vepain/spyprogmanager@TAG
```
where `TAG` is the tag version.

### Local

For the more last commit:
```bash
git clone https://gitlab.com/vepain/spyprogmanager.git
cd spyprogmanager
python3.X -m pip install .
```

Usage
-----

### Basic use

```python
# -*- coding: utf-8 -*-

"""Example program."""

from spyprogmanager import ProgramManager

manager = ProgramManager()

# Add a command line argument
# ---------------------------
#   all the dictionnary keys are optionnals arguments
#   in argparse.ArgumentParser.add_argument method
cmdline_opt = (
    ('-i', '--myint'),
    {
        'dest': 'myint',
        'default': 0,
        'type': int,
        'help': 'An int',
    },

manager += cmdline_opt

# CRUCIAL: parse command line arguments
# -------------------------------------
manager.compute_cmdline()

# Get value of 'myint' parsed with argparse
opt_value = manager['myint']
```

### Use of markdown log file

```python
# -*- coding: utf-8 -*-

"""Example program."""

from spyprogmanager import ProgramManager

manager = ProgramManager()

# CRUCIAL: parse command line arguments
# -------------------------------------
manager.compute_cmdline()

# Use log file
# ------------
manager.verb_log('# Header 1\n')
manager.verb_date()
manager.verb_log()
manager.verb_options()

if manager.verb():
    manager.verb_log(f'The output directory path: {manager.out_dir()}')
```

If the command line was `myprog.py -o ./result --verbose`, and the date was `2021/03/24` you will obtain a file `./result/log_2020_03_04.md` in which it is written:

````markdown
# Header 1

2020-03-04

```bash
--outdir /absolute_path/result
--verbose
```

The output directory path: `/absolute_path/result`
````

Next
----

- [ ] TODO: add markdown functions
- [ ] TODO: tests and code cover
  - [ ] tests
  - [ ] coverage online
- [ ] TODO: build better doc with sphinx
    - [ ] better doc
    - [ ] better HTML/LaTeX doc

- [ ] FIXME: add all argparse.ArgumentParser.add_argument keywords in parameters

- [ ] XXX: how to deal with argparse methods?

- [ ] IDEA: verb relative path from out_dir
- [ ] IDEA: use subprogram manager in program manager
  - [ ] do a subclass for subprogram manager and mainprogram manager?