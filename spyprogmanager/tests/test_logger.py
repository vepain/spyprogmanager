# -*- coding=utf-8 -*-

"""Test logger part."""

# pylint: disable=redefined-outer-name,missing-param-doc,missing-yield-doc


from spyprogmanager.markdown_logger import MarkdownLogger
from spyprogmanager.utils.path_utils import Path


def test_nonempty_init():
    """Init object with path."""
    path = './tmp_log.md'
    MarkdownLogger(path)


def test_write(md_logger: MarkdownLogger):
    """Test write in logger."""
    msg = 'message'
    md_logger.write(msg)
    with open(md_logger.logfile_path, 'r') as log:
        l_lines = log.readlines()
    assert l_lines == [f'{msg}\n']


def test_set_logfile_path(md_logger: MarkdownLogger):
    """Test set log filepath."""
    new_path = Path('./tmp_log')
    md_logger.logfile_path = new_path
    assert md_logger.logfile_path == Path(new_path)
