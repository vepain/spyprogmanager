# -*- coding=utf-8 -*-

"""Test logger part."""

# pylint: disable=redefined-outer-name, missing-param-doc,
# pylint: disable=protected-access, missing-yield-doc


from spyprogmanager.prog_manager import ProgramManager


def test_verb_log(manager: ProgramManager):
    """Test verb in log."""
    msg = 'lol'
    manager.verb_log(msg)
    with open(manager.logfile_path(), 'r') as fin:
        line = fin.readline()
        print(f'DEBUG: {line}')
        assert line == 'lol\n'
