# -*- coding=utf-8 -*-

"""Conftest file for pytest."""

# pylint: disable = missing-yield-doc, redefined-outer-name, missing-return-doc

import sys
from argparse import ArgumentParser, Namespace
from contextlib import suppress
from typing import Generator

import pytest

from spyprogmanager.markdown_logger import MarkdownLogger
from spyprogmanager.parameters import ArgParamContainer
from spyprogmanager.prog_manager import ProgramManager


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
OPT = 'option'
OPT_LONG = '--option'
DEFAULT = 0
HELP = 'the help'

TRUE_OPT = 'true_opt'
TRUE_LONG = '--true'
TRUE_HELP = 'True help'

NO_HELP = 'No help'
SHORT_NO_OPT = '-n'


# ============================================================================ #
#                                   FIXTURES                                   #
# ============================================================================ #
@pytest.fixture
def md_logger() -> Generator[MarkdownLogger, None, None]:
    """Instantiate a MarkdownLogger object."""
    # Arrange
    empty_md_logger = MarkdownLogger()
    yield empty_md_logger
    # Cleanup
    with suppress(FileNotFoundError):
        del empty_md_logger.logfile_path


@pytest.fixture
def empty_argparam() -> Generator[ArgParamContainer, None, None]:
    """Instantiate an empty ArgParamContainer."""
    # Arrange
    sys.argv = ['']
    empty_param = ArgParamContainer()
    yield empty_param
    # Cleanup
    del empty_param


@pytest.fixture
def argparam() -> Generator[ArgParamContainer, None, None]:
    """Instantiate a ArgParamContainer."""
    # Arrange
    arg = arg_parser()
    argparam = ArgParamContainer()
    argparam.add_argument(OPT, getattr(arg, OPT), OPT_LONG)
    argparam.add_argument(TRUE_OPT, getattr(arg, TRUE_OPT), TRUE_LONG)
    argparam.add_argument(NO_HELP, getattr(arg, NO_HELP), SHORT_NO_OPT)
    yield argparam
    # Cleanup
    del argparam


@pytest.fixture
def manager() -> Generator[ProgramManager, None, None]:
    """Instantiate a MarkdownLogger object."""
    # Arrange
    sys.argv = ['']  # because of passing argument from test env
    manager = ProgramManager()
    yield manager
    # Cleanup
    with suppress(FileNotFoundError, TypeError):
        manager.rm_log()


# ============================================================================ #
#                                   FUNCTIONS                                  #
# ============================================================================ #
def arg_parser() -> Namespace:
    """Return parsed command line."""
    sys.argv = [
        'myprog',
        OPT_LONG,
        '1',
        TRUE_LONG,
        SHORT_NO_OPT,
    ]
    argparser = ArgumentParser()
    argparser.add_argument(
        OPT_LONG, dest=OPT, default=DEFAULT, help=HELP, type=int,
    )
    argparser.add_argument(
        TRUE_LONG, dest=TRUE_OPT, action='store_true', help=TRUE_HELP,
    )
    argparser.add_argument(
        SHORT_NO_OPT, dest=NO_HELP, action='store_false',
    )
    return argparser.parse_args()
