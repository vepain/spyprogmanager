# -*- coding=utf-8 -*-

"""Test parameters part."""

# pylint: disable=redefined-outer-name, missing-yield-doc
# pylint: disable=protected-access, missing-param-doc


from spyprogmanager.parameters import ArgParamContainer
from spyprogmanager.tests.conftest import OPT, OPT_LONG, TRUE_LONG


def test_to_md_str(argparam: ArgParamContainer):
    """Test to md str."""
    md_str = (
        '```bash\n'
        '# COMMAND LINE ARGUMENTS\n'
        f'{OPT_LONG} {argparam[OPT]}\n'
        f'{TRUE_LONG}\n'
        '```\n'
    )
    assert argparam.to_md_str() == md_str
    name = 'PARAMNAME'
    value = True
    argparam.add_parameter(name, value)
    md_str = (
        '```bash\n'
        '# COMMAND LINE ARGUMENTS\n'
        f'{OPT_LONG} {argparam[OPT]}\n'
        f'{TRUE_LONG}\n'
        '# PARAMETERS\n'
        f'{name}={value}\n'
        '```\n'
    )
    assert argparam.to_md_str() == md_str
