# -*- coding: utf-8 -*-

"""Type function definition for argparse."""

from argparse import ArgumentTypeError
from spyprogmanager.utils.path_utils import Path


# ============================================================================ #
#                                  PATH TYPES                                  #
# ============================================================================ #

def path_like(argument: str) -> Path:
    """path_like type definition."""
    return Path(argument)


def valid_path(argument: str) -> Path:
    """valid_path type definition.

    Parameters
    ----------
    argument : str
        The argument given before being parsed.

    Returns
    -------
    Path
        The absolute path of an existing file / directory.

    Raises
    ------
    ArgumentTypeError
        If the path does not point to an existing file / directory.
    """
    path = Path(argument)
    if not path.exists():
        raise ArgumentTypeError(
            f'{argument} is not the path of an existing file / directory',
        )
    return path


# ============================================================================ #
#                                   INT TYPES                                  #
# ============================================================================ #

def positive_int(argument: int) -> int:
    """positive_int type definition.

    Parameters
    ----------
    argument : string
        The argument given before being parsed.

    Returns
    -------
    int
        A positive int.

    Raises
    ------
    ArgumentTypeError
        If it cannot be converted to int or if it is not a positive int.
    """
    try:
        value = int(argument)
        if value >= 0:
            return value
    except ValueError:
        pass
    raise ArgumentTypeError(f'{argument} is not a positive int')


def strict_positive_int(argument: int) -> int:
    """strict_positive_int type definition.

    Parameters
    ----------
    argument : string
        The argument given before being parsed.

    Returns
    -------
    int
        A strictly positive int.

    Raises
    ------
    ArgumentTypeError
        If it cannot be converted to int or if it is not a strictly positive \
    int.
    """
    try:
        value = int(argument)
        if value > 0:
            return value
    except ValueError:
        pass
    raise ArgumentTypeError(f'{argument} is not a strictly positive int')


# ============================================================================ #
#                                  FLOAT TYPES                                 #
# ============================================================================ #

def positive_float(argument: float) -> float:
    """positive_float type definition.

    Parameters
    ----------
    argument : string
        The argument given before being parsed.

    Returns
    -------
    int
        A positive float.

    Raises
    ------
    ArgumentTypeError
        If it cannot be converted to float or if it is not a positive float.
    """
    try:
        value = float(argument)
        if value >= 0:
            return value
    except ValueError:
        pass
    raise ArgumentTypeError(f'{argument} is not a positive float')
