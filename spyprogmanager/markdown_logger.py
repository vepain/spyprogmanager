# -*- coding: utf-8 -*-

"""Markdown Logger module."""

from __future__ import annotations, division

from datetime import date

from spyprogmanager.utils.path_utils import Path, rm


# ============================================================================ #
#                                CONCRETE CLASS                                #
# ============================================================================ #

class MarkdownLogger():
    """MarkdownLogger class."""

    def __init__(self, logfile_path: Path = None):
        """Initializer."""
        self.logfile_path = (
            logfile_path if logfile_path is not None
            else Path(f'./log_{date.today().strftime("%y_%m_%d")}.md')
        )

    # ~*~ Property ~*~

    @property
    def logfile_path(self) -> Path:
        """Return the log file path.

        Returns
        -------
        Path
            Log file path
        """
        return self._logfile_path

    @logfile_path.setter
    def logfile_path(self, log_filepath: Path):
        """Set the log filepath.

        Parameters
        ----------
        log_filepath : str or Path
            The log file path
        """
        self._logfile_path = log_filepath

    @logfile_path.deleter
    def logfile_path(self):
        """Clear log file."""
        rm(self._logfile_path)

    def write(self, msg: str):
        """Print the mesage and write it in the log file.

        Parameters
        ----------
        msg : str
            Message to print
        """
        print(msg)
        with open(self._logfile_path, 'a') as log:
            log.write(f'{msg}\n')
