# -*- coding: utf-8 -*-

"""Parameters utils."""

from __future__ import annotations, division

from datetime import date
from resource import RUSAGE_CHILDREN, RUSAGE_SELF, getrusage
from time import time
from typing import Any

from spyprogmanager.markdown_logger import MarkdownLogger
from spyprogmanager.parameters import ArgParamContainer
from spyprogmanager.utils.path_utils import Path


# ============================================================================ #
#                                CONCRETE CLASS                                #
# ============================================================================ #
class ProgramManager():
    """Class for managing user command line options, parameters and log file.

    The manager stores arguments' value obtained by user command line, stores
    parameters during program, and give you some methods for print informations
    in a logger file.

    This way, you can easily group all data you product in an unique output
    directory and keep track of the program's action and track of results in the
    logger file.

    Examples
    --------
    **The beginning: parse the command line**

    Create an empty manager object

    >>> manager = ProgramManager()

    You can add a command line argument like that:

    >>> arg_opt = (
    ...     ('-o', '--option-long'),
    ...     {
    ...         'dest': 'option_dest',
    ...         'default': 0,
    ...         'type': int,
    ...         'help': 'this is the help for this argument.',
    ...     },
    ... )
    >>> manager += arg_opt

    All the field in the dictionnary correspond to argparse field when you use
    the `argparse.ArgumentParser.add_argument` method.

    Then you must tell to the manager that you finish to describe all the
    command line arguments so that it can parse the whole command line:

    >>> manager.compute_cmdline()

    **Command line argument value access**

    Like the parameters you will add, you can access to command line argument's
    value this way:

    >>> opt_value = manager['option_dest']

    By default, the main output directory path and the boolean verbose are in
    command line argument

    >>> out_dir_value = manager.out_dir()
    >>> to_verb_value = manager.verb()

    **Verb to the log file**

    The following lines:

    >>> manager.verb_log('# Header 1')
    >>> manager.verb_log()
    >>> manager.verb_date()
    >>> manager.verb_log()
    >>> manager.verb_options()
    >>> if manager.verb():
    ...     manager.verb_log(f'The output directory path: {manager.out_dir()}')

    will give you the example log file in the `README.md`
    """

    def __init__(self, logfile_path: Path = None, verbose: bool = True):
        """The Initializer."""
        self.__date = date.today()
        self.__start_time = time()
        self.__argsparam = ArgParamContainer()
        self.__logger = MarkdownLogger(logfile_path)
        self.__verb = verbose

    def verb_log(self, msg: str = ''):
        """Print the message and write it in the log file.

        Parameters
        ----------
        msg : str, optional
            Message to print in log, by default ''
        """
        self.__logger.write(msg)

    def verb_options(self):
        """Display the options set."""
        self.__logger.write(self.__argsparam.to_md_str())

    def verb_usage(self):
        """Display and write in prog log the time / memory usage."""
        if self.__verb:
            ru_utime = (
                getrusage(RUSAGE_CHILDREN).ru_utime
                + getrusage(RUSAGE_SELF).ru_utime
            )
            ru_maxrss = (
                getrusage(RUSAGE_CHILDREN).ru_maxrss
                + getrusage(RUSAGE_SELF).ru_maxrss
            )
            self.__logger.write(f'- user CPU time: {ru_utime:.3f} sec.')
            self.__logger.write(f'- real time: {self.get_time():.3f} sec.')
            self.__logger.write(f'- Peak RSS: {ru_maxrss / 1000000:.3f} GB')

    def get_time(self) -> float:
        """Return the consummed time since the beginning.

        Returns
        -------
        float
            Time in seconds since the beginning
        """
        return time() - self.__start_time

    def verb_date(self):
        """Display and write in prog log the today date."""
        if self.verb():
            self.__logger.write(f'date: {self.__date}\n')

    def rm_log(self):
        """Remove log file."""
        del self.__logger.logfile_path

    def verb(self) -> bool:
        """Return True if verbose option.

        Returns
        -------
        bool
            True if verbose option
        """
        return self.__verb

    def logfile_path(self) -> Path:
        """Return log file path.

        Returns
        -------
        Path
            Log file path
        """
        return self.__logger.logfile_path

    # ~*~ Setter ~*~

    def add_argument(self, name: str, value: Any, longopt: str):
        """Add argument entry.

        Parameters
        ----------
        name : str
            Argument name
        value : Any
            Argument value
        longopt : str
            Argument long option name
        """
        self.__argsparam.add_argument(name, value, longopt)

    def add_parameter(self, name: str, value: Any):
        """Add parameter entry.

        Parameters
        ----------
        name : str
            Parameter's name
        value : Any
            Parameter's value
        """
        self.__argsparam.add_parameter(name, value)

    # ~*~ Special methods ~*~

    def __getitem__(self, name: str) -> Any:
        """Return the value of option from its name.

        Parameters
        ----------
        name : str
            Argument / parameter's name

        Returns
        -------
        Any
            Argument / parameter's value
        """
        return self.__argsparam[name]
