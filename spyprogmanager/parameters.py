# -*- coding: utf-8 -*-

"""Parameters utils.

# IDEA: method for json parsed used command line opt
"""

from __future__ import annotations, division

from typing import Any


# ============================================================================ #
#                                CONCRETE CLASS                                #
# ============================================================================ #
class ArgParamContainer():
    """Container class for arguments and parameters."""

    def __init__(self):
        """Initialize object."""
        # Command line args parsed
        #   name -> [long_option, value]
        self.__argname_index: dict[str, int] = {}
        self.__arglongopt: list[str] = []
        self.__argvalue: list[Any] = []
        # Params given in code
        #   name -> value
        self.__paramname_value: dict[str, Any] = {}

    # ~*~ Getter ~*~

    def argument(self, name: str) -> Any:
        """Return argument value from its name.

        Parameters
        ----------
        name : str
            Argument name

        Returns
        -------
        Any
            Argument value

        Raises
        ------
        UnknowParamNameError
            In case name does not correspond to any recorded argument
        """
        try:
            ind = self.__argname_index[name]
        except KeyError as exc:
            raise UnknowParamNameError(name) from exc
        return self.__argvalue[ind]

    def parameter(self, name: str) -> Any:
        """Return parameter value from its name.

        Parameters
        ----------
        name : str
            Parameter name

        Returns
        -------
        Any
            Parameter value

        Raises
        ------
        UnknowParamNameError
            In case name does not correspond to any recorded parameter
        """
        try:
            return self.__paramname_value[name]
        except KeyError as exc:
            raise UnknowParamNameError(name) from exc

    # ~*~ Setter ~*~

    def add_argument(self, name: str, value: Any, longopt: str):
        """Add argument entry.

        Parameters
        ----------
        name : str
            Argument name
        value : Any
            Argument value
        longopt : str
            Argument long option name
        """
        self.__argname_index[name] = len(self.__argname_index)
        self.__argvalue.append(value)
        self.__arglongopt.append(longopt)

    def add_parameter(self, name: str, value: Any):
        """Add parameter entry.

        Parameters
        ----------
        name : str
            Parameter's name
        value : Any
            Parameter's value
        """
        self.__paramname_value[name] = value

    # ~*~ String method ~*~

    def to_md_str(self) -> str:
        """Return as a string the options and their value.

        Returns
        -------
        str
            Markdown formatted list of args and params
        """
        format_str = '```bash\n'
        if self.__arglongopt:
            format_str += '# COMMAND LINE ARGUMENTS\n'
            for ind, longopt in enumerate(self.__arglongopt):
                # IDEA: what about the options order ?
                if isinstance(self.__argvalue[ind], bool):
                    if self.__argvalue[ind]:
                        format_str += f'{longopt}\n'
                else:
                    format_str += f'{longopt} {self.__argvalue[ind]}\n'
        if self.__paramname_value:
            format_str += '# PARAMETERS\n'
            for name, value in self.__paramname_value.items():
                format_str += f'{name}={value}\n'
        return format_str + '```\n'

    def __getitem__(self, name: str) -> Any:
        """Return the value of argument / parameter from its name.

        Parameters
        ----------
        name : str
            Argument or parameter name

        Returns
        -------
        Any
            Value corresponding to name

        Raises
        ------
        UnknowParamNameError
            In case name does not correspond to any recorded arg or param
        """
        try:
            ind = self.__argname_index[name]
            return self.__argvalue[ind]
        except KeyError:
            try:
                return self.__paramname_value[name]
            except KeyError as exc:
                raise UnknowParamNameError(name) from exc


# ============================================================================ #
#                                   EXCEPTION                                  #
# ============================================================================ #
class UnknowParamNameError(Exception):
    """UnknowParamNameError exception subclass."""

    def __init__(self, name: str):
        """The Initializer."""
        super().__init__()
        self.__name = name

    def __str__(self) -> str:
        """String representation of exception raised.

        Returns
        -------
        str
            Exception message
        """
        return (
            'ERROR: there is neither command line argument,'
            f' nor parameter given by code named {self.__name}'
        )
