#!/usr/bin/env bash

# USAGE:
#   ./config/install_venv39.sh

python3.9 -m venv .venv_39
source ./.venv_39/bin/activate
if [[ -n "$VIRTUAL_ENV" ]]; then
    pip install wheel # necessary to install like `python setup.py bdist_wheel`
    pip install -r requirements/requirements-docs.txt
    pip install -r requirements/requirements-tests.txt
    pip install -r requirements/requirements-linters.txt
else
    echo "ERROR: virtual environment is not installed, retry!"
    rm -rf .venv_39 2>/dev/null
fi
